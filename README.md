# php-getting-started

A barebones PHP app that makes use of the [Silex](http://silex.sensiolabs.org/) web framework, which can easily be deployed to Heroku.

This application supports the [Getting Started with PHP on Heroku](https://devcenter.heroku.com/articles/getting-started-with-php) article - check it out.

## Getting Started

* Install Heroku CLI : https://devcenter.heroku.com/articles/heroku-command-line
* Run ‘heroku login' in Terminal to login to the apps.
* Run `git remote add stage https://git.heroku.com/call-center-leads-staging.git`
* Run `git remote add prod https://git.heroku.com/call-center-leads-prod.git`

Deploy bitbucket staging branch to Heroku git repo by running pushing to stage

Deploy bitbucket prod branch to Heroku git repo by running pushing to prod

## Heroku Deploying

Install the [Heroku Toolbelt](https://toolbelt.heroku.com/).

```sh
$ git clone git@github.com:heroku/php-getting-started.git # or clone your own fork
$ cd php-getting-started
$ heroku create
$ git push heroku master
$ heroku open
```

or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentation

For more information about using PHP on Heroku, see these Dev Center articles:

- [Getting Started with PHP on Heroku](https://devcenter.heroku.com/articles/getting-started-with-php)
- [PHP on Heroku](https://devcenter.heroku.com/categories/php)
